async function sp_microsoft_teams_get_call_handler() {
    let call = null;
    let peopleService = null;

    try {
        let stage = angular.element(
            document.getElementsByTagName('calling-stage')[0]
        );
        let controller = stage.controller();
        call = controller.call;
        peopleService = controller.$scope.app.peopleService;
    } catch {
        call = null;
    }

    // Listar Participantes
    let participants = {};

    if (call != null) {
        let myself = call.currentUserSkypeIdentity;
        let mymri = call.callerMri;
        participants[mymri] = {
            'name': myself.displayName,
            'id': mymri
        };

        for (i = 0; i < call.participants.length; i++) {
            let user = call.participants[i];
            participants[user.mri] = {
                'id': user.mri,
                'name': user.displayName
            };
        }

        let mris = Object.keys(participants);
        let profiles = await peopleService.getAllPeopleProfile(mris);

        for (i = 0; i < profiles.length; i++) {
            if (participants.hasOwnProperty(profiles[i].mri)) {
                participants[profiles[i].mri].profile = profiles[i];
            }
        }
    }

    let event = new CustomEvent('sp_microsoft_teams_get_call', {
        detail: {
            'participants': participants
        }
    });

    document.dispatchEvent(event);
};

sp_microsoft_teams_get_call_handler();