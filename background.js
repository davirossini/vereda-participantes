chrome.runtime.onInstalled.addListener(function() {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {


        // TEAMS
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: {
                    hostEquals: 'teams.microsoft.com'
                },
            })],
            actions: [
                new chrome.declarativeContent.ShowPageAction(),
            ]
        }]);
    });
});

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.action == "download") {
            let msg = request.message;

            // Data 
            let now = new Date();

            // Nome do Arquivo (CHAMADA + DD/MM/AAAA)
            let filename = 'Chamada - ' +
            now.getDate().toString().padStart(2, "0") + '-' +
            (now.getMonth() + 1).toString().padStart(2, "0") + '-' +
            now.getFullYear()  + '-' + now.getHours() + 'h' + now.getMinutes() + 'm' + '.txt';

            var blob = new Blob([msg], {
                type: "text/plain"
            });
            var url = URL.createObjectURL(blob);
            chrome.downloads.download({
                url: url,
                filename: filename
            });

            sendResponse({
                status: 'completed'
            });
        }
    }
);